﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Core;
using BusinessLogic.Core.ActionFilter;
using BusinessLogic.DBModel;

namespace WebApplication1.Controllers
{

    [AdminMod]
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DeleteUser(int id)
        {
            UserRoleContext db = new UserRoleContext();

            // delete user
            db.Database.ExecuteSqlCommand("DELETE FROM users WHERE ID=@id",
                    new System.Data.SqlClient.SqlParameter("@id", id));

            // delete user cart
            db.Database.ExecuteSqlCommand("DELETE FROM CartItems WHERE UserId=@id",
                   new System.Data.SqlClient.SqlParameter("@id", id));

            return RedirectToAction("Index");
        }
    }
}