﻿using System;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class LogOutController : Controller
    {
        // GET: LogOut
        public ActionResult Index()
        {
            if (Request.Cookies["UserData"] != null)
            {
                HttpCookie cookie = new HttpCookie("UserData");
                cookie.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}