﻿using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class CustomExceptionController : Controller
    {
        public ActionResult Forbidden()
        {
            return View();
        }
    }
}