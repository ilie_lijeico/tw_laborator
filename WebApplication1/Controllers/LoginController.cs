﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using BusinessLogic;
using BusinessLogic.Interfaces;
using WebApplication1.Models;
using Domain.Entities.User;
using BusinessLogic.DBModel;
using UserRole = BusinessLogic.Core.UserRole;
using BusinessLogic.Core;

namespace WebApplication1.Controllers
{
    public class LoginController : Controller
    {
        //private UserContext db = new UserContext();
        private readonly ISession _session;

        public LoginController()
        {
            var bl = new MyBusinessLogic();
            _session = bl.GetSessionBL();
        }

        //GET: Login
        public ActionResult Index()
        {
            string role = UserProperties.getRole();
            ViewBag.Role = role;

            UserRoleContext db = new UserRoleContext();

            if (role == "User")
            {
                var cartItems = db.cart_items.ToList();
                if (cartItems != null)
                {
                    ViewBag.CartItems = cartItems;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserLogin login)
        {
            if (ModelState.IsValid)
            {
                ULoginData data = new ULoginData
                {
                    Email = login.Email,
                    Password = login.Password,
                    LoginIp = Request.UserHostAddress,
                    LoginDateTime = DateTime.Now
                };
                var userLogin = _session.UserLogin(data);
                
                if (userLogin.Status)
                {
                    //ADD COOKIE
                    HttpCookie cookie = new HttpCookie("UserData");
                    cookie["UserName"] = userLogin.UserName;
                    cookie["UserId"] = userLogin.ID.ToString();
                    cookie.Expires = DateTime.Now.AddMonths(1);
                    Response.Cookies.Add(cookie);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", userLogin.StatusMsg);
                    ViewBag.Message = userLogin.StatusMsg;
                    return View();
                }
            }

            return View();
        }
    }
}