﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Core;
using BusinessLogic.Core.ActionFilter;
using BusinessLogic.DBModel;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            string role = UserProperties.getRole();
            ViewBag.Role = role;

            UserRoleContext db = new UserRoleContext();

            if (role == "User")
            {
                var cartItems = db.cart_items.ToList();
                if (cartItems != null)
                {
                    ViewBag.CartItems = cartItems;
                }
            }

            var articles = (from p in db.articles
                            orderby p.ID descending
                            select p).Take(3).ToList();
            
            if (articles != null)
            {
                ViewBag.articles = articles;
            }

            return View();
        }
    }
}