﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Core;
using BusinessLogic.DBModel;

namespace WebApplication1.Controllers
{
    public class ContactUsController : Controller
    {
        // GET: ContactUs
        public ActionResult Index()
        {
            string role = UserProperties.getRole();
            ViewBag.Role = role;

            UserRoleContext db = new UserRoleContext();

            if (role == "User")
            {
                var cartItems = db.cart_items.ToList();
                if (cartItems != null)
                {
                    ViewBag.CartItems = cartItems;
                }
            }

            return View();
        }
    }
}