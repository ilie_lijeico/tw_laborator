﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic.Core;
using BusinessLogic.Core.ActionFilter;
using BusinessLogic.DBModel;
using BusinessLogic.DBModel.ArticleModel;
using BusinessLogic.DBModel.CartModel;
using System.Web.Helpers;
using System.Data.SqlClient;

namespace WebApplication1.Controllers
{
    public class ArticlesController : Controller
    {
        // GET: Articles
        public ActionResult Index(string articleAction = "")
        {
            UserRoleContext db = new UserRoleContext();

            string role = UserProperties.getRole();

            ViewBag.Role = role;

            if (articleAction == "deleted")
            {
                ViewBag.Action = "deleted";
            }

            if (role == "User")
            {
                var cartItems = db.cart_items.ToList();
                if (cartItems != null)
                {
                    ViewBag.CartItems = cartItems;
                }
            }

            return View(db.articles);
        }

        [AdminMod]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(Article form, HttpPostedFileBase Image)
        {
            string articleImage = "default.png";

            if (Image != null)
            { 
                string fileName = Crypto.Hash(System.IO.Path.GetFileName(Image.FileName)) + ".png";
                string SaveLocation = Server.MapPath("~/Content/Articles/images/" + fileName);
                try
                {
                    Image.SaveAs(SaveLocation);
                    articleImage = fileName;
                }
                catch (Exception ex)
                {
                    Response.Write("Error: " + ex.Message);
                }
            }

            UserRoleContext db = new UserRoleContext();

            string articleText = form.Text;
            string articleTitle = form.Title;
            string articlePrice = form.Price;

            Article article = new Article
            {
                Text = articleText,
                Title = articleTitle,
                Image = articleImage,
                Price = articlePrice
            };

            db.articles.Add(article);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [AdminMod]
        public ActionResult Delete(int id)
        {
            UserRoleContext db = new UserRoleContext();

            var article = db.articles.Find(id);

            if (article != null)
            {
                // delete article
                db.articles.Remove(article);

                // delete article dependencies in CartItems table
                db.Database.ExecuteSqlCommand("DELETE FROM CartItems WHERE ArticleId=@id", 
                    new SqlParameter("@id", id));

                db.SaveChanges();
            }

            //return View("Index", db.articles);
            return RedirectToAction("Index", new { articleAction = "deleted" });
        }

        [UserMod]
        public ActionResult AddCart(int id)
        {
            UserRoleContext db = new UserRoleContext();

            int userId = UserProperties.getId();

            var articleId = id;

            CartItem cartItem = new CartItem { ArticleId = articleId, UserId = userId };

            db.cart_items.Add(cartItem);
            db.SaveChanges();

            var referer = Request.UrlReferrer.ToString();

            return Redirect(referer);
        }

        [AdminMod]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Article form, HttpPostedFileBase Image)
        {
            UserRoleContext db = new UserRoleContext();

            var articleImage = (from s in db.articles
                                where s.ID == form.ID
                                select s.Image).ToArray();

            if (Image != null)
            {
                string fileName = Crypto.Hash(System.IO.Path.GetFileName(Image.FileName)) + ".png";
                string SaveLocation = Server.MapPath("~/Content/Articles/images/" + fileName);
                try
                {
                    var result = db.articles.SingleOrDefault(b => b.ID == form.ID);
                    if (result != null)
                    {
                        result.Image = fileName;
                        db.SaveChanges();
                    }
                    Image.SaveAs(SaveLocation);
                    //articleImage = fileName;
                }
                catch (Exception ex)
                {
                    Response.Write("Error: " + ex.Message);
                }
            }

            var resultArticle = db.articles.SingleOrDefault(b => b.ID == form.ID);
            if (resultArticle != null)
            {
                resultArticle.Title = form.Title;
                resultArticle.Text = form.Text;
                resultArticle.Price = form.Price;
                db.SaveChanges();
            }

            var referer = Request.UrlReferrer.ToString();
            return RedirectToAction("Index", new { articleAction = "edited" });
        }
    }
}