﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using BusinessLogic.Core;
using BusinessLogic.DBModel;
using BusinessLogic.Interfaces;
using Domain.Entities.User;

namespace WebApplication1.Controllers
{
    public class RegistrationController : Controller
    {
        private readonly ISession _session;

        public RegistrationController()
        {
            var bl = new MyBusinessLogic();
            _session = bl.GetSessionBL();
        }

        UserContext db = new UserContext();

        // GET: Registration
        public ActionResult Index()
        {
            string role = UserProperties.getRole();
            ViewBag.Role = role;

            UserRoleContext db = new UserRoleContext();

            if (role == "User")
            {
                var cartItems = db.cart_items.ToList();
                if (cartItems != null)
                {
                    ViewBag.CartItems = cartItems;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(URegistrationData data)
        {
            if (ModelState.IsValid)
            {
                var userRegistration = _session.UserRegistration(data);

                /*if (userRegistration.Status)
                {
                }
                else
                {
                    ModelState.AddModelError("", userRegistration.StatusMsg);
                    ViewBag.Message = userRegistration.StatusMsg;
                }#1#*/

                ViewBag.Status = userRegistration.Status;
                ViewBag.Message = userRegistration.StatusMsg;
            }


            return View();
        }
    }
}