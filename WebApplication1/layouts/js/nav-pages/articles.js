﻿$(function () {
    var seeMoreWidth = "";

    $('body').on('click', '#add-article', function () {
        $("#add-article-form").submit();
    });

    bsCustomFileInput.init();

    $('body').on('mouseenter', 'a.price', function () {
        //var price = $(this).parent().find('a.price-value').text();
        $(this).hide();
        seeMoreWidth = $(this).width();
        $(this).parent().find('a.price-value').show();
        $(this).parent().find('a.price-value').css('width', seeMoreWidth + 'px');
        //console.log(seeMoreWidth);
    });

    $('body').on('mouseleave', 'a.price-value', function () {
        $(this).hide();
        $(this).parent().find('a.price').show();
        // console.log('fdsfs');
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#articleImageEdit").change(function () {
        readURL(this);
    }); 	

    $('body').on('click', 'a.edit-article', function () {
        var articleId = $(this).parents('.card').data('articleid');
        var articleTitle = $(this).parents('.card').find('.card-title').text();
        var articleText = $(this).parents('.card').find('.card-text').text();
        var articlePrice = $(this).parents('.card').find('.price-value').data('price');
        var articleImageSelector = $(this).parents('.card').find('img.card-img-top').attr('src');

        $('#editModal').find('input[name="ID"]').val(articleId);
        $('#editModal').find('input[name="Title"]').val(articleTitle);
        $('#editModal').find('textarea[name="Text"]').val(articleText);
        $('#editModal').find('input[name="Price"]').val(articlePrice);
        $('#img-upload').attr('src', articleImageSelector);
    });

    $('#edit-article').on('click', function (e) {
        $(this).parents('form').submit();
    });
});