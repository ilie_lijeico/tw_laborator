﻿using System.Web.Mvc;
using System.Web.Routing;

namespace WebApplication1
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Articles",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Articles", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login",
                url: "{controller}/{action}",
                defaults: new { controller = "Articles", action = "Index" }
            );

            routes.MapRoute(
                name: "ContactUs",
                url: "{controller}/{action}",
                defaults: new { controller = "Articles", action = "Index" }
            );

            routes.MapRoute(
                name: "Registration",
                url: "{controller}/{action}",
                defaults: new { controller = "Articles", action = "Index" }
            );
            
            routes.MapRoute(
                name: "PassRecover",
                url: "{controller}/{action}",
                defaults: new { controller = "PassRecover", action = "Index" }
            ); 
            
            routes.MapRoute(
                name: "AdminPanel",
                url: "{controller}/{action}",
                defaults: new { controller = "Home", action = "Index" }
            );
        }
    }
}
