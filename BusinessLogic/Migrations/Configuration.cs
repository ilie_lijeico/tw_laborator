﻿namespace BusinessLogic.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;
    using BusinessLogic.DBModel;
    using BusinessLogic.DBModel.ArticleModel;
    using BusinessLogic.DBModel.CartModel;

    internal sealed class Configuration : DbMigrationsConfiguration<BusinessLogic.DBModel.UserRoleContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            //Database.SetInitializer<UserRoleContext>(new CreateDatabaseIfNotExists<UserRoleContext>());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<UserRoleContext>());
        }

        protected override void Seed(BusinessLogic.DBModel.UserRoleContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            //context.Database.CreateIfNotExists();

            var New_User = new List<user>
            {
                new user{FirstName="Ilies",LastName="Lijeico",
                    Email="skatervan@gmail.com", City="Chisinau",
                    Country="Moldova", Password="12345678", Role=1},
                new user{FirstName="Vladislav",LastName="Gaiduc",
                    Email="gaiduc@gmail.com", City="Orhei",
                    Country="Marea Britanie", Password="12345678", Role=2},
                new user{FirstName="Scutelnic",LastName="Cristian",
                    Email="scutelnic@gmail.com", City="Rezina",
                    Country="Romania", Password="12345678", Role=3},
                new user{FirstName="Test1",LastName="User1",
                    Email="test@gmail.com", City="Chisinau",
                    Country="Moldova", Password="12345678", Role=2},
            };
            New_User.ForEach(ns => context.users.AddOrUpdate(p => p.Email, ns));
            context.SaveChanges();

            var User_Roles = new List<user_roles>
            {
                new user_roles{Role="Admin"},
                new user_roles{Role="User"},
                new user_roles{Role="Guest"},
            };
            User_Roles.ForEach(ns => context.user_roles.AddOrUpdate(p => p.Role, ns));
            context.SaveChanges();

            var articles = new List<Article>
            {
            new Article{Title="First Article Title",Text="First Article Text",Image="default.png" },
            new Article{Title="Second Article Title",Text="Second Article Text",Image="default.png" },
            new Article{Title="Third Article Title",Text="Third Article Text", Image="default.png" },
            };

            articles.ForEach(s => context.articles.AddOrUpdate(s));
            context.SaveChanges();

            var cart_items = new List<CartItem>
            {
            new CartItem{ ArticleId=5, UserId=2 },
            new CartItem{ ArticleId=7, UserId=2 },
            new CartItem{ ArticleId=5, UserId=4 },
            };

            cart_items.ForEach(s => context.cart_items.AddOrUpdate(s));
            context.SaveChanges();
        }
    }
}
