﻿using System.Web;
using System.Web.Mvc;

namespace BusinessLogic.Core.ActionFilter
{
    public class AdminModAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //filterContext.HttpContext.Response.Write("OnActionExecuted");
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string role = "Guest";

            var cookie = HttpContext.Current.Request.Cookies["UserData"];
            if (cookie != null)
            {
                string UserName = cookie["UserName"];
                int UserId = int.Parse(cookie["UserId"]);

                BusinessLogic.Core.UserRole userRole = new UserRole();
                role = userRole.GetUserRole(UserId);
            }

            if (role != "Admin")
            {
                var Url = new UrlHelper(filterContext.RequestContext);
                var url = Url.Action("Forbidden", "CustomException");
                filterContext.Result = new RedirectResult(url);
            }
        }
    }
}
