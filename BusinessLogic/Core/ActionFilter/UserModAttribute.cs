﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BusinessLogic.Core;

namespace BusinessLogic.Core.ActionFilter
{
    public class UserModAttribute : FilterAttribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string role = UserProperties.getRole();

            if (role != "User")
            {
                var Url = new UrlHelper(filterContext.RequestContext);
                var url = Url.Action("Forbidden", "CustomException");
                filterContext.Result = new RedirectResult(url);
            }
        }
    }
}
