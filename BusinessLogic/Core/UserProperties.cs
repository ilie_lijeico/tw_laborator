﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DBModel;

namespace BusinessLogic.Core
{
    public static class UserProperties
    {
        public static UserRoleContext db = new UserRoleContext();

        public static List<user> getAllUsers()
        {
            List<user> users = new List<user>();

            users = (from s in db.users
                        where s.Role != 1 select s).ToList();

            return users;
        }

        public static string getRoleById(int id)
        {
            string role = "Guest";

            var aux_role = (from s in db.user_roles
                    where s.ID == id
                    select s.Role).FirstOrDefault();

            if(aux_role != null)
            {
                role = aux_role;
            }

            return role;
        }

        // get user role by accessing cookie
        public static string getRole()
        {
            string role = "Guest";

            var cookie = System.Web.HttpContext.Current.Request.Cookies["UserData"];

            if (cookie != null)
            {
                int UserId = int.Parse(cookie["UserId"]);

                UserRoleContext db = new UserRoleContext();

                role = (from s in db.user_roles
                               join sa in db.users on s.ID equals sa.Role
                               where sa.ID == UserId
                               select s.Role).FirstOrDefault();

                if (role == null)
                {
                    role = "Guest";
                }
            }

            return role;
        }

        public static int getId()
        {
            int id = 0;

            var cookie = System.Web.HttpContext.Current.Request.Cookies["UserData"];

            if (cookie != null)
            {
                id = int.Parse(cookie["UserId"]);
            }

            return id;
        }

        public static List<DBModel.CartModel.UserCartItem> getCart()
        {
            List<DBModel.CartModel.UserCartItem> cartItems = new List<DBModel.CartModel.UserCartItem>();

            var UserId = getId();

            cartItems = db.articles.Join(db.cart_items.Where(x => x.UserId == UserId), // второй набор
                p => p.ID, // свойство-селектор объекта из первого набора
                c => c.ArticleId, // свойство-селектор объекта из второго набора
                (p, c) => new DBModel.CartModel.UserCartItem // результат
                {
                    Title = p.Title,
                    Text = p.Text,
                    Image = p.Image,
                    Datetime = c.Datetime,
                    Status = c.Status,
                    Price = p.Price
                }).ToList();

            return cartItems;
        }

        public static user getUser()
        {
            var userId = getId();
            user user = new user();
            user = db.users.Find(userId);

            return user;
        }
    }
}
