﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DBModel;
using System.Data.SqlClient;
using System.Web;

namespace BusinessLogic.Core
{
    public class UserRole
    {
        public string Role { get; set; }

        public string GetUserRole(int id)
        {
            UserRoleContext db = new UserRoleContext();

            string role = (from s in db.user_roles
                join sa in db.users on s.ID equals sa.Role
                where sa.ID == id
                select s.Role).FirstOrDefault();

            if (role == null)
            {
                role = "Guest";
            }

            /*var role = db.user_roles
                .SqlQuery("Select user_roles.Role from user_roles INNER JOIN users ON user_roles.ID = users.Role AND users.ID = ").FirstOrDefault();
                */

            return role;
        }
    }
}