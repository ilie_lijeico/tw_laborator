﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DBModel;
using Domain.Entities.User;

namespace BusinessLogic.Core
{
    public class UserApi
    {
        internal ULoginResp UserLoginAction(ULoginData data)
        {
            UserContext db = new UserContext();

            string email = data.Email;
            string password = data.Password;

            if (email == null || password == null)
            {
                return new ULoginResp
                {
                    Status = false,
                    StatusMsg = "*Please fill up required fields",
                    UserName = null,
                    ID = 0
                };
            }

            var user = db.users
                .SqlQuery("SELECT * FROM users WHERE Password=@password AND Email=@email", new SqlParameter("@password", password), new SqlParameter("@email", email))
                .FirstOrDefault();

            if (user == null)
            {
                return new ULoginResp
                {
                    Status = false,
                    StatusMsg = "*The Username or password is not correct",
                    UserName = null,
                    ID = 0
                };
            }

            return new ULoginResp { Status = true, StatusMsg = "You have been successfully logged in", UserName = user.FirstName, ID = user.ID };
        }

        internal URegistrationResp UserRegistrationAction(URegistrationData data)
        {
           try {
                UserContext db = new UserContext();

                string email = data.Email;

                if (db.users.Any(users => users.Email == email))
                {
                    return new URegistrationResp
                    {
                        Status = false,
                        StatusMsg = "*This email is already taken"
                    };
                }

                user user = new user
                {
                    Email = data.Email,
                    Password = data.Password,
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    Country = data.Country,
                    City = data.City,
                    Role = 2
                };

                db.users.Add(user);
                db.SaveChanges();
            } catch (DbEntityValidationException e) {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        return new URegistrationResp { Status = false, StatusMsg = ve.ErrorMessage };
                    }
                }


            }

            return new URegistrationResp { Status = true, StatusMsg = "You have been successfully signed up" };
        }

    }
}
