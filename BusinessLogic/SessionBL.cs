﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Core;
using BusinessLogic.Interfaces;
using Domain.Entities.User;

namespace BusinessLogic
{
    public class SessionBL : UserApi, ISession
    {
        public ULoginResp UserLogin(ULoginData data)
        {
            return UserLoginAction(data);
        }

        public URegistrationResp UserRegistration(URegistrationData data)
        {
            return UserRegistrationAction(data);
        }
    }
}
