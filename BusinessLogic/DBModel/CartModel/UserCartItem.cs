﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.DBModel.CartModel
{
    public class UserCartItem
    {
        public string Text { get; set; }

        public string Title { get; set; }

        public string Image { get; set; }

        public int Status { get; set; }

        public DateTime Datetime { get; set; }

        public string Price { get; set; }
    }
}
