﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLogic.DBModel.CartModel
{
    public class CartItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public int ArticleId { get; set; }

        [Required]
        public int Status { get; set; } = 0;

        [Required]
        public DateTime Datetime { get; set; } = DateTime.Now;
    }
}
