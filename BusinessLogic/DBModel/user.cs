//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLogic.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class user
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Username cannot be shorter than 5 and longer than 30 characters.")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 5, ErrorMessage = "Username cannot be shorter than 5 and longer than 30 characters.")]
        public string LastName { get; set; }

        [Required]
        [StringLength(30)]
        public string Email { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 8, ErrorMessage = "Password might have minimum 8 characters.")]
        public string Password { get; set; }

        [Required]
        public int Role { get; set; }
    }
}
