﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace BusinessLogic.DBModel.ArticleModel
{
    public class ArticleContext : DbContext
    {
        public ArticleContext()
            : base("name=UserContext")
        {
        }

        public DbSet<Article> articles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
