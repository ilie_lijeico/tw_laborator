﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.DBModel.ArticleModel
{
    public class Article
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Username cannot be shorter than 1 and longer than 50 characters.")]
        public string Title { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Username cannot be shorter than 1 and longer than 255 characters.")]
        public string Text { get; set; }

        public string Image { get; set; } = "default.png";

        public string Price { get; set; } = "999.99";
    }
}
