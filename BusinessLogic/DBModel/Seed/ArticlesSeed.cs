﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DBModel.ArticleModel;
using System.Data.Entity.Migrations;

namespace BusinessLogic.DBModel.Seed
{
    public class ArticlesSeed : System.Data.Entity.DropCreateDatabaseIfModelChanges<ArticleContext>
    {
        protected override void Seed(ArticleContext context)
        {
            var articles = new List<Article>
            {
            new Article{Title="First Article Title",Text="First Article Text" },
            new Article{Title="Second Article Title",Text="Second Article Text" },
            new Article{Title="Third Article Title",Text="Third Article Text" },
            };

            articles.ForEach(s => context.articles.AddOrUpdate(s));
            context.SaveChanges();
        }
    }
}
